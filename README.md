# Observer

This repository contains the android project of Observer application. 
The observer monitors per application CPU usage and system Battery Health. 
This application is also capable of writing the data to system's external memory.
Check the user manual file for quick-start and further details about this application.
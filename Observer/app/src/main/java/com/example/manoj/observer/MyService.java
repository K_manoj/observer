package com.example.manoj.observer;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MyService extends Service {
    public MyService() {
    }

    private String packageName;
    private String appName;
    private Handler h = new Handler();
    private int delay = 2000; //2 seconds
    private Runnable runnable;
    private String betteryLevel;
    private String timeStamp_filename = String.valueOf(System.currentTimeMillis());

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }
    /**Starts the service here*/
    @Override
    public void onCreate() {
        super.onCreate();
        Toast.makeText(this, "Saving data to download folder", Toast.LENGTH_LONG).show();
    }

    /**This is saving CPU usage and battery percentage every 2 seconds*/
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Bundle extras = intent.getExtras();
        assert extras != null;
        packageName = extras.getString("pname");
        appName = extras.getString("appname");
        loadBatteryInfo();
        h.postDelayed(new Runnable() {
            @Override
            public void run() {
                writeData();
                runnable = this;
                h.postDelayed(runnable, delay);
            }
        }, delay);
        return super.onStartCommand(intent, flags, startId);
    }

    /**Below Three functions are responsible to fetch battery status from phone*/
    private void loadBatteryInfo(){
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Intent.ACTION_POWER_CONNECTED);
        intentFilter.addAction(Intent.ACTION_POWER_DISCONNECTED);
        intentFilter.addAction(Intent.ACTION_BATTERY_CHANGED);
        registerReceiver(batteryInfoReceiver,intentFilter);
    }

    private void updateBatteryData(Intent intent){
        boolean present = intent.getBooleanExtra(BatteryManager.EXTRA_PRESENT, false);
        if(present){
            int level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
            betteryLevel = level+"%";
        }
        else {
            betteryLevel = "Battery not present";
        }
    }


    private BroadcastReceiver batteryInfoReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            updateBatteryData(intent);
        }
    };

    /**This function writes data to download folder of external memory of the phone, Creates new file on every run where filename is CPU_<current Datetime>*/
    private void writeData(){
        StringBuilder cpuinfo = new StringBuilder();
        Process p;
        String command = "top -n 1 -s cpu";
        try {
            p = Runtime.getRuntime().exec(command);
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(p.getInputStream()));
            String line = "";
            while ((line = reader.readLine()) != null) {
                if(line.toLowerCase().contains(packageName.toLowerCase())){
                    cpuinfo.append(line).append("\n");
                    p.waitFor();
                }
            }
        }
        catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        String keyword = "%";
        String rx = "\\w+\\w*" + keyword;
        String filename;
        Pattern pat = Pattern.compile(rx);
        Matcher matcher = pat.matcher(cpuinfo);
        String timeStamp;
        if (matcher.find()) {
            timeStamp = new SimpleDateFormat("yyyy:MM:dd , HH:mm:ss", Locale.getDefault()).format(Calendar.getInstance().getTime());
            String content = String.format("%s,%s,%s,%s\n", appName, matcher.group(0), timeStamp, betteryLevel);
            File file;
            FileOutputStream outputStream;
            try {
                filename = String.format("CPU_%s.csv", timeStamp_filename);
                file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), filename);
                outputStream = new FileOutputStream(file, true);
                outputStream.write(content.getBytes());
                outputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        else{
            timeStamp = new SimpleDateFormat("yyyy:MM:dd , HH:mm:ss", Locale.getDefault()).format(Calendar.getInstance().getTime());
            String content = String.format("%s,0%%,%s\n", appName, timeStamp);
            File file;
            FileOutputStream outputStream;
            try {
                filename = String.format("CPU_%s.csv", timeStamp_filename);
                file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), filename);
                outputStream = new FileOutputStream(file, true);
                outputStream.write(content.getBytes());
                outputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        Toast.makeText(this, "Stopped saving data", Toast.LENGTH_SHORT).show();
    }
}

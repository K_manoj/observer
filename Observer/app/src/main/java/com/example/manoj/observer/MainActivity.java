package com.example.manoj.observer;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

public class MainActivity extends AppCompatActivity {

    private ListView lv;
    private ArrayList<String> aa = new ArrayList<>();
    final int MY_PERMISSIONS_WRITE_EXTERNAL_STORAGE = 1;
    private ArrayAdapter<String> adapter;
    private String pname;
    private Intent myintent;
    private Bundle extras;
    private String selectedFromList;

    /**Arrange list items to alphabetical order*/
    Comparator<String> ALPHABETICAL_ORDER1 = new Comparator<String>() {
        public int compare(String object1, String object2) {
            return String.CASE_INSENSITIVE_ORDER.compare(object1, object2);
        }
    };


    /**Fetch the applications info from the phone and add them to array*/
    public void getpackagename(){
        final List<PackageInfo> packages = getPackageManager().getInstalledPackages(0);
        for(PackageInfo pack : packages)
        {
            if (!(isSystemPackage(pack)) || pack.toString().contains("google")) {
                String appName = pack.applicationInfo.loadLabel(getPackageManager()).toString();
                aa.add(appName);
            }
        }


        /**Arrange list items to alphabetical order*/
        Collections.sort(aa, ALPHABETICAL_ORDER1);
        adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, aa);
        lv.setAdapter(adapter);
        lv.setFastScrollEnabled(true);

        /**Onclick listener on the list items*/
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                selectedFromList = (String) lv.getItemAtPosition(position);
                for(PackageInfo pack : packages)
                {
                    String name = pack.applicationInfo.loadLabel(getPackageManager()).toString();
                    if(Objects.equals(name, selectedFromList)){
                        pname = pack.packageName;
                        myintent = new Intent(MainActivity.this,display.class);
                        extras = new Bundle();
                        extras.putString("package",pname);
                        extras.putString("pname",selectedFromList);
                        try{
                            myintent.putExtras(extras);;
                            startActivityForResult(myintent,1);

                        }
                        catch(ActivityNotFoundException ex){
                            Toast.makeText(view.getContext(), "Oops! Something went wrong", Toast.LENGTH_LONG).show();
                            startActivityForResult(myintent,1);
                        }
                    }
                }
            }
        });
    }


    /**Permissions are handled here*/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(
                Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_WRITE_EXTERNAL_STORAGE);
        } else {
            // Android version is lesser than 6.0 or the permission is already granted.
            setContentView(R.layout.activity_main);
            lv = findViewById(R.id.lv);
            getpackagename();
        }
    }

    /**Permissions' result are handled here*/
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_WRITE_EXTERNAL_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    setContentView(R.layout.activity_main);
                    lv = findViewById(R.id.lv);
                    getpackagename();
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.b
                    Log.v("Log", "Permission not Granted");

                }
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    /**Search functionality is implemented here*/
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_search, menu);
        MenuItem item = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) item.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.getFilter().filter(newText);
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }

    /**checks if package is system installed or user installed*/
   private boolean isSystemPackage(PackageInfo pkgInfo) {
        return ((pkgInfo.applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM) != 0);
    }
}

package com.example.manoj.observer;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.TextView;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.Viewport;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class display extends Activity {
    private TextView result;
    private Handler h = new Handler();
    int delay = 2000; //2 seconds
    private Runnable runnable;
    private String pack;
    private String name;
    private LineGraphSeries<DataPoint> series;
    private int lastx = 0;
    private double y;
    private GraphView graph;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display);
        result = findViewById(R.id.mtv4);

        /** Create realtime graph*/
        graph = findViewById(R.id.graph);
        series = new LineGraphSeries<>();
        series.setColor(R.color.yellow);
        graph.addSeries(series);
        Viewport viewport = graph.getViewport();
        viewport.setYAxisBoundsManual(true);
        viewport.setMinY(0);
        viewport.setMaxY(100);
        viewport.setScrollable(true);

        /** Intent to recieve the app name and package*/
        Intent myintent = getIntent();
        Bundle extras = myintent.getExtras();
        assert extras != null;
        pack = extras.getString("package");
        name = extras.getString("pname");
        loadBatteryInfo();
        loadCpuInfo();

    }
    /** This function find the CPU percentage using linux top command
     *  and set the value in text view. Also add it to the graph coordinates*/
    private void loadCpuInfo(){
        StringBuilder cpuinfo = new StringBuilder();
        TextView percent = findViewById(R.id.mtv2);
        Process p;
        String command = "top -n 1 -s cpu";
        try {
            p = Runtime.getRuntime().exec(command);
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(p.getInputStream()));
            String line = "";
            while ((line = reader.readLine()) != null) {
                if(line.toLowerCase().contains(pack.toLowerCase())){
                    cpuinfo.append(line).append("\n");
                    p.waitFor();
                }
            }
        }
        catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        String keyword = "%";
        String rx = "\\w+\\w*" + keyword;
        Pattern pat = Pattern.compile(rx);
        Matcher matcher = pat.matcher(cpuinfo);
        if (matcher.find()) {
            percent.setText(matcher.group(0));
            String s = matcher.group(0);

            /**Adding value of Y to graph*/
            y = Double.parseDouble(s.replace("%", ""));
            series.appendData(new DataPoint(lastx++, y),true,10);
        }
        else{
            percent.setText("0%");
            /**Adding value of Y to graph*/
            series.appendData(new DataPoint(lastx++, 0),true,10);
        }

    }

    /**This function fetch the battery info of phone and build a String from it*/
    private void loadBatteryInfo(){

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Intent.ACTION_POWER_CONNECTED);
        intentFilter.addAction(Intent.ACTION_POWER_DISCONNECTED);
        intentFilter.addAction(Intent.ACTION_BATTERY_CHANGED);
        registerReceiver(batteryInfoReceiver,intentFilter);
    }

    /* Checks if external storage is available for read and write
    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    Checks if external storage is available to at least read
    public boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            return true;
        }
        return false;
    }*/

    /**This function fetch the battery info of phone and build a String from it*/
    private void updateBatteryData(Intent intent){
        boolean present = intent.getBooleanExtra(BatteryManager.EXTRA_PRESENT, false);
        if(present){
            StringBuilder batteryinfo = new StringBuilder();
            int health = intent.getIntExtra(BatteryManager.EXTRA_HEALTH, 0);
            if(health == 1){
                batteryinfo.append("Health : UNKNOWN").append("\n");
            }
            if(health == 2){
                batteryinfo.append("Health : GOOD").append("\n");
            }
            if(health == 3){
                batteryinfo.append("Health : OVERHEAT").append("\n");
            }
            if(health == 4){
                batteryinfo.append("Health : DEAD").append("\n");
            }
            if(health == 5){
                batteryinfo.append("Health : OVER VOLTAGE").append("\n");
            }
            if(health == 6){
                batteryinfo.append("Health : UNSPECIFIED").append("\n");
            }
            if(health == 7){
                batteryinfo.append("Health : COLD").append("\n");
            }

            int level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
            //int scale = intent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);

            batteryinfo.append("Level :").append(level).append("% \n");

            /*if(level != -1 && scale != -1){
                int plugged = intent.getIntExtra(BatteryManager.EXTRA_PLUGGED,0);
                if(plugged == 2){
                    batteryinfo.append("Plugged: Yes").append("\n");
                }else{
                    batteryinfo.append("Plugged: No").append("\n");
                }


                int Status = intent.getIntExtra(BatteryManager.EXTRA_PRESENT, -1);
                if(Status== -1)
                    batteryinfo.append("Status: Battery is present").append("\n");
            }*/

            int temperature = intent.getIntExtra(BatteryManager.EXTRA_TEMPERATURE,0);

            if(temperature > 0){
                batteryinfo.append("Temperature : ").append((float) temperature / 10f).append("C \n");
            }

            int voltage = intent.getIntExtra(BatteryManager.EXTRA_VOLTAGE, 0);
            batteryinfo.append("Voltage :").append(voltage).append("mV\n");


            result.setText(batteryinfo.toString());
        }
    }

    private BroadcastReceiver batteryInfoReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            updateBatteryData(intent);
        }
    };

    /**This is on click function to start the service*/
    public void startService(View v){

        Intent serviceIntent = new Intent(this, MyService.class);
        Bundle serviceExtra = new Bundle();
        serviceExtra.putString("pname",pack);
        serviceExtra.putString("appname",name);
        serviceIntent.putExtras(serviceExtra);
        startService(serviceIntent);

    }
    /**This is on click function to stop the service*/
    public void stopService(View v){
        Intent serviceIntent = new Intent(this, MyService.class);
        stopService(serviceIntent);
    }

    /**This functions runs the function after every 2 seconds*/
    @Override
    protected void onResume() {
        h.postDelayed(new Runnable() {
            @Override
            public void run() {
                loadCpuInfo();
                graph.addSeries(series);
                runnable = this;
                h.postDelayed(runnable, delay);
            }
        }, delay);
        super.onResume();

    }

    @Override
    protected void onPause() {
        h.removeCallbacks(runnable);
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }
}